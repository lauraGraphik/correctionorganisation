<?php

namespace App\Controller;

use App\Repository\EvenementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front")
     */
    public function index( EvenementRepository $repoEvenement ): Response
    {
        $evenements=$repoEvenement->findAll();
        $evenementsPasses=$repoEvenement->findEvenementsPasses();
        dump($evenements);
        return $this->render('front/index.html.twig', [
            'evenements' => $evenements,
        ]);
    }
}
